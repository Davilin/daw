<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Ex 6</title>
    </head>
    <body>
        <h1>Ex 6</h1>
        <?php
        $x = 320;
        $con = 20;
        do {
            if ($con == 20) {
                echo "<p>" . $x . "</p>";
                $con = 0;
            }
            $con++;
            $x--;
        } while ($x >= 160);
        ?>
    </body>
</html>