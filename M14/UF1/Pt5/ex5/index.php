<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Ex 5</title>
    </head>
    <body>
        <h1>Ex 5</h1>
        <?php
        $x = 320;
        $con = 20;
        while ( $x >= 160) {
            if ($con == 20) {
                echo "<p>" . $x . "</p>";
                $con = 0;
            }
            $x--;
            $con++;
        }
        ?>
    </body>
</html>