<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Ex 3</title>
    </head>
    <body>
        <h1>Ex 3</h1>
        <?php
        $x = 0;
        do {
            if ($x % 5 == 0) {
                echo "<p>" . $x . " es multipe de 5.</p>";
            }
            $x++;
        } while ($x <= 100);
        ?>
    </body>
</html>