<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Ex 1</title>
    </head>
    <body>
        <h1>Ex 1</h1>
        <?php
        for ($x = 0; $x <= 100; $x++) {
            if ($x % 5 == 0) {
                echo "<p>" . $x . " es multipe de 5.</p>";
            }
        }
        ?>
    </body>
</html>