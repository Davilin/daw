<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Ex 2</title>
    </head>
    <body>
        <h1>Ex 2</h1>
        <?php
        $x = 0;
        while ( $x <= 100) {
            if ($x % 5 == 0) {
                echo "<p>" . $x . " es multipe de 5.</p>";
            }
            $x++;
        }
        ?>
    </body>
</html>